""" 
All common imports, settings, parameters
"""


BIOGRAPHIES_DIR = '/../data/INLP_FINAL/Bio_clean/'

classification_output = '/verb_classifications.csv'
tfidf_output = '/word_tfidf.csv'
occupational_verbs_output = '/data_occupations/'

occupational_fraction_threshold = 0.15
common_words_fraction_threshold = 0.15
df_idf_threshold = 0
max_overlapping_occupations = 20 # in how many occupations a verb can appear (in final sets, after filtering)
max_personal_count = 1 # in how many documents can a "personal" verb appear