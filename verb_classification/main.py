""" 
App used to classify verbs into 3 classes:
- biographical
- occupational
- personal

the data source is a structure of JSON files, divided to directories"""

import common
import load
import process
from sketches.dictionary_sorter import *
import logging
import save
import tfidf

if __name__ == "__main__":
    
    logging.basicConfig(level = logging.INFO)
    
    words_in_articles = load.load_all_content()
    verbs_in_articles = process.filter_verbs(words_in_articles)
    # verb counts for each occupation -> article
    verb_counts_in_articles = process.count_words_for_biographies(verbs_in_articles)

    # verb counts for each occupation
    occupation_verb_counts = merge_verbs(verb_counts_in_articles)
    # for each occupation: sorted list of pairs (verb, count) sorted by the count, descending
    sorted_verbs = sort_verbs(occupation_verb_counts)

    # all occupations merged - summary 
    merged_verb_counts = merge_all_verbs(occupation_verb_counts)
    sorted_merged_verbs = sort_merged_verbs(merged_verb_counts)

    # idf values for each word
    idf = tfidf.verbs_idf(occupation_verb_counts, merged_verb_counts)
    # tf-idf values for each occupation and word
    occupational_tfidf = tfidf.occupations_verbs_tfidf(occupation_verb_counts, idf)
    # for each occupation compute sets of words with TFIDF lower than given threshold
    lowest_tfidf_for_occupations = tfidf.lowest_tfidf_for_occupations(occupational_tfidf, common.df_idf_threshold)

#     print "after filtering commons:"
#     print occupational_sets


    tfidf_sorted_pairs = {}
    for occupation, verbs_tfs in occupational_tfidf.iteritems():
        pairs = [(verb, tf) for verb, tf in verbs_tfs.iteritems()]
        sorted_pairs = sorted(pairs, key= lambda x: x[1], reverse=True)
        tfidf_sorted_pairs[occupation] =[item[0] for item in sorted_pairs]

    top_tf_fraction = 0.1
    occupational_sets = {}
    for occupation, sorted_verbs in tfidf_sorted_pairs.iteritems():
        count = int(len(sorted_verbs) * top_tf_fraction)
        occupational_sets[occupation] = set(sorted_verbs[0:count])



    #filter words with low tf_idf from occupational lists
    process.delete_words_according_to_occupation(occupational_sets, lowest_tfidf_for_occupations)
    
#     print "after filtering low tf-idf from occupations"
#     print occupational_sets
    
    #count how in how many occupations each verb appears (IN THE TOP VERBS)
    verbs_counts_in_occupations = {}
    for occupation, verb_set in occupational_sets.iteritems():
        for verb in verb_set:
            try:
                verbs_counts_in_occupations[verb] += 1
            except KeyError:
                verbs_counts_in_occupations[verb] = 1

    verbs_in_many_occupations = set([verb for (verb, count) in verbs_counts_in_occupations.iteritems() \
                                     if count > common.max_overlapping_occupations])
#     print "counts in occupations:"
#     print verbs_counts_in_occupations
#     print "verbs to delete (in many occ.):"
#     print verbs_in_many_occupations
#     
    
    # delete verbs that appear in more than X occupations
    process.delete_words(occupational_sets, verbs_in_many_occupations)
#     print occupational_sets

    # computing in how many documents each verb appears
    word_to_documents = {}
    for occupation, documents in verb_counts_in_articles.iteritems():
        for document in documents:
            for word in document:
                if word not in word_to_documents:
                    word_to_documents[word] = 0
                word_to_documents[word] += 1

    ##words that are specific to a certain person only, they appear only in a single document
    personal_set = set([word for word, doc_count in word_to_documents.iteritems() \
                        if doc_count <= common.max_personal_count])
    personal_list = sorted(personal_set)

    ##delete verbs that are personal from occupational sets
    process.delete_words(occupational_sets, personal_set)
    # its the final version of occupational sets

    logging.info("occupational counts per occupation:")

    occupational_set = set()
    for occupation, words in occupational_sets.iteritems():
        occupational_set.update(words)
        logging.info(occupation + ": " + str(len(words)))
    occupational_list = sorted(occupational_set)

    all_words_set = set(sorted_merged_verbs)
    biographical_list = sorted(all_words_set - occupational_set - personal_set)
    classes = {
        "occupational": occupational_list,
        "personal": personal_list,
        "biographical": biographical_list
    }
    
    logging.info("occupational/personal/biographical count")
    logging.info(str([len(occupational_list), len(personal_list), len(biographical_list)]))
    
    save.write_classification(classes)
    save.write_tfidf(occupational_tfidf)
    filtered_occupational_verbs = process.filter_occupational_verbs(words_in_articles, occupational_set)
    save.write_occupational_verbs(filtered_occupational_verbs)



    
