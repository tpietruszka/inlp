"""
Various processing functions for the text corpus
"""

def is_verb(entry):
    """
    Receives an entry in a form: ["word", "POStag", "lemma"]
    Return bool - is it a verb?
    """
    return entry[1][0] == "V"

def filter_verbs(occupations):
    """ filters a structure of:
    dictionary(occupations) -> list of biographies -> list of words
    to verbs-only
    """
     
    filtered = {}
    for occupation in occupations.keys():
        for occupation_pairs in occupations[occupation]:
            for words in occupation_pairs['content']:
                for word in words:
                    if is_verb(word):
                        if occupation not in filtered:
                            filtered[occupation] = []
                        filtered[occupation].append(word[0])
        #filtered[occupation] = [[word for word in biography if is_verb(word)] for biography in occupations[occupation]['content']]
    return filtered


def filter_occupational_verbs(words_in_articles, occupational_set):
    filtered = {}
    for occupation in words_in_articles.keys():
        for word_pairs in words_in_articles[occupation]:
            verbs = set()
            file_code_name = word_pairs['file_code_name']
            for words in word_pairs['content']:
                for word in words:
                    if is_verb(word):
                        verbs.add(word[0])
            occupational_verbs = verbs.intersection(occupational_set)
            filtered[file_code_name] = sorted(list(occupational_verbs))
    return filtered

def count_words(word_list):
    """ counts words in a list, returns a dictionary with counts"""
    counts = {}
    for word in word_list:
        #word = entry[0]
        word = word.lower()
        try:
            counts[word] += 1
        except KeyError:
            counts[word] = 1
    return counts


def count_words_for_biographies(occupations):
    counts_occupations = {}
    for occupation in occupations:
        counts_occupations[occupation] = []

        counts = count_words(occupations[occupation])
        counts_occupations[occupation].append(counts)
        
    return counts_occupations
        
        
def delete_words(occupational_sets, words_to_delete):
    for occupation in occupational_sets.keys():
        occupational_sets[occupation] = occupational_sets[occupation] - words_to_delete


def delete_words_according_to_occupation(occupational_sets, words_to_delete):
    for occupation in occupational_sets.keys():
        if occupation in words_to_delete:
            occupational_sets[occupation] = occupational_sets[occupation] - words_to_delete[occupation]