'''
Loads biographical articles from JSON files, 
directories correspond to occupation names, 
each file correspond to one article
'''
import os
import json
import logging
import common

current_folder = os.getcwd()
biographies_path = current_folder+common.BIOGRAPHIES_DIR


def load_all_content():
    """ returns a dictionary of:
    occupations -> list of files -> list of words
    """
    folders = [d for d in os.listdir(biographies_path) if os.path.isdir(os.path.join(biographies_path, d))]
    output = {}
    i = 0
    j = 0
    #For all the folders
    for folder in folders:
        # folder is the occupation
        filelist = os.listdir(biographies_path+folder)
        logging.info('\n\n'+folder+' with '+str(len(filelist))+' files')
        #if i > 1000:
        #    break
        #i += 1
        output_list = []
        #For all the files in folder
        for item in filelist:
            #item is a name of the json file

            if not item.endswith('.json'):
                continue
            #if j > 1000:
            #    break
            #j += 1
            file_to_content = {}

            json_file = biographies_path+folder+'/'+item

            file_code_name = folder + '_' + item
            file_to_content['file_code_name'] = file_code_name
            file_to_content['content'] = []
            #Open and read the json
            try:
                json_data=open(json_file)
                data = json.load(json_data)
            
                words = []
                
                for content in data:
                    for sentence in content["content"]: 
                        
                        if type(sentence) is dict: # complex element - a name etc
                            for element in sentence["content"]:
                                words.append(element)
                        else:  # simple word
                            words.append(sentence)
#                         print sentence0
                file_to_content['content'].append(words)
            except ValueError as e:
                logging.warning("Error while reading JSON file " + folder + "/" + item + "\n" + e.message)
            output_list.append(file_to_content)

        output[folder] = output_list
            
    return output

if __name__ == "__main__":
    d = load_all_content() 
       
            
