"""
Functions used to save results to CSV files
"""
import os
import common

def write_classification(classes):
    """ Saves dictionary of classes -> words to a csv file
    """
    current_folder_path = os.getcwd()
    with open(current_folder_path + common.classification_output, 'w') as f:
        for clazz, words in classes.iteritems():
            for w in words:
                try:
                    f.write(w.decode('utf8') + ',' + clazz.decode('utf8'))
                    f.write('\n')
                except:
                    continue
    f.close()

def write_tfidf(tfidf_dictionary):
    """ writes a (nested) dictionary of occupation -> word -> tfidf to a CSV file
    """
    current_folder_path = os.getcwd()
    with open(current_folder_path + common.tfidf_output, 'w') as f:
        for occupation, word_tfidf in tfidf_dictionary.iteritems():
            for word, tfidf in word_tfidf.iteritems():
                f.write(occupation.encode('utf-8','replace') + ',' + word.encode('utf-8','replace') + ',' + str(tfidf).encode('utf-8','replace'))
                f.write('\n')
    f.close()


def write_occupational_verbs(occupational_verbs):
    """ writes occupational words to a CSV file
    """
    current_folder_path = os.getcwd()

    for file_code_name, verb in occupational_verbs.iteritems():
        with open(current_folder_path + common.occupational_verbs_output + file_code_name + '.csv', 'w') as f:
            for v in verb:
                f.write(v.encode('utf-8','replace'))
                f.write('\n')
        f.close()
