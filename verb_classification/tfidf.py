"""
Functions connected to tf-idf calculations
"""
import logging
import math

def verbs_idf(occupation_verb_counts, merged_verb_counts):
    """ Computes IDF of each word found in merged_verb_counts
    treating occupations as single documents
    """
    idf = {}
    occupation_count = len(occupation_verb_counts.keys())
    for verb in merged_verb_counts.keys():
        # in how many occupations does the verb appear
        occupations_with_verb = 0
        for occupation, counts in occupation_verb_counts.iteritems():
            if verb in counts:
                occupations_with_verb += 1
        try:
            idf[verb] = math.log(occupation_count / occupations_with_verb)
        except ZeroDivisionError:
            idf[verb] = 1
            logging.warning("verb from merged list not found in any occupation - something went wrong")
    return idf

def occupations_verbs_tfidf(occupation_verb_counts, idf):
    """computes TFIDF for each verb in each occupation
    (each occupation treated as a single document)
    """
    occupational_tfidf = {}
    for occupation, verb_list in occupation_verb_counts.iteritems():
        occupational_tfidf[occupation] = {}
        for word, count in verb_list.iteritems():
            occupational_tfidf[occupation][word] = count * idf[word]
    return occupational_tfidf
def lowest_tfidf_for_occupations(occupational_tfidf, threshold):
    """ Returns sets of words with tfidf lower than threshold for each occupation
    """
    lowest_tfidf_verbs = {}
    for occupation, words in occupational_tfidf.iteritems():
        lowest_tfidf_verbs[occupation] = set()
        for word,tf_idf in words.iteritems():
            if tf_idf <= threshold:
                lowest_tfidf_verbs[occupation].add(word)
    return lowest_tfidf_verbs