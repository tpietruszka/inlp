"""
Provides a dummy function returning sample dictionary of:
 occupations-> lists of articles -> dictionaries of verbs -> counts
"""

def verb_counts_for_articles():
    d = {
    'artists': 
    [
        {
            'be': 5,
            'paint': 3,
            'die': 14,
        },
        {
            'live': 3,
            'paint': 10,
            'be': 99
        }
    ],
    'politicians':
    [
        {
            'failed': 5,
            'resigned': 3,
            'promised': 4
        },
        {
            'be': 1000,
            'worked': 3,
            'served': 10, 
            'die': 15
        }
    ], 
    'dancers':
    [
        {
            'danced': 10, 
            'died': 10, 
            'eat': 5,
            'drink': 3
        }
    ]
    }
    return d;
