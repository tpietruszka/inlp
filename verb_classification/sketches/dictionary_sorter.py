""" Helper functions for dealing with word-count dictionaries
"""

def get_sorted_occupations(verb_counts_articles):
    """ Returns a list of occupation names, 
    provided a dictionary with occupations as keys
    """
    return sorted(verb_counts_articles.keys())


def merge_verbs(verb_counts_articles):
    """ Gets word-counts for each occupation and article in it, 
    Returns summarized word-counts for occupation only
    """
    biography_verbs_merged = {}

    for occupation, articles in verb_counts_articles.iteritems():
        biography_verbs_merged[occupation] = {}
        for article in articles:
            for word, count in article.iteritems():
                try: # add the count of article-word pair if  it had a value
                    biography_verbs_merged[occupation][word] += count
                except KeyError: # create the value if it did not
                    biography_verbs_merged[occupation][word] = count
    return biography_verbs_merged


def sort_verbs(bibliography_verbs_merged):
    """ For each occupation (in dictionary) sorts words according to their count
    Returns a list of pairs for each occupation
    """
    sorted_verbs = {}
    for key in bibliography_verbs_merged:
        verb_dictionary = bibliography_verbs_merged[key]
#         print('verb_dictionary: ')
#         print(verb_dictionary)
        sorted_pairs = sorted(verb_dictionary.items(), key= lambda x: x[1], reverse=True)
#         print('sorted_pairs: ')
#         print(sorted_pairs)
        sorted_verb_list = list()
        for k in sorted_pairs:
            sorted_verb_list.append(k[0])
        sorted_verbs[key] = sorted_verb_list
#     print('sorted_verbs: ')
#     print(sorted_verbs)
    return sorted_verbs


def merge_all_verbs(bibliography_verbs_merged):
    """ Merges word-counts from per-occupation to one summary
    """
    all_verbs_merged = {}
    for key in bibliography_verbs_merged:
        values = bibliography_verbs_merged[key]
        for k in values:
            prev_count = 0
            if k in all_verbs_merged:
                prev_count = all_verbs_merged[k]
            all_verbs_merged[k] = values[k] + prev_count

    return all_verbs_merged


def sort_merged_verbs(bibliography_verbs_merged):
    """ Sorts words according to their count
    Returns a list of pairs (word, count)
    """
    sorted_pairs = sorted(bibliography_verbs_merged.items(), key= lambda x: x[1], reverse=True)
#     print('sorted_pairs: ')
#     print(sorted_pairs)
    sorted_verb_list = list()
    for k in sorted_pairs:
        sorted_verb_list.append(k[0])
    return sorted_verb_list