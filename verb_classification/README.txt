Running inlp/verb_classification/main.py will produce three csv files:

-- word_tfidf.csv - verbs are assigned a an occupational label (politicians, artists, etc) and calculated tf-idf value. This list is subsequently used in the occupational
classifier.

-- verb_classifications.csv - initial approach - the verbs are given one of the three labels:
     * occupational
     * personal
     * biographical

    Data sources:
    The input documents that were used to extract the verbs are located under inlp/data/INLP_FINAL/Bio_clean/ and are provided in JSON format.
    If some non-verbs appear in the verb_classification.csv, it is due to the incorrect tagging of the words in the input files.

    Algorithm:
    The main logic flow for classification is provided in main.py. The classification of verbs follows these steps:
     * A per-occupation list of words is created. Occurences of words in their respective occupations are counted.
     * A list of top words for each occupation is created. The threshold for the top words per occupation is given in common.occupational_fraction_threshold
     * A list of common verbs across all occupation is generated.
     * Top words are extracted from this list (see common.common_words_fraction_threshold)
     * These top words are substracted from each occupational list
     * A list of personal words (words that only come up in one document) is built.
     * The list of occupational verbs is reduced further to exclude the personal verbs.
     * Three final lists are generated:
       * personal (verbs occuring only in one document)
       * occupational (top verbs in the occupation, minus common verbs, minus personal verbs)
       * biographical (all the rest of the verbs)

-- data_occupations/*.txt.json.csv
    * used for debugging / analysis of the verb_classifications.csv