import os 
import sys

#-------------------------------------------------------------
#     Read the non-biographic
#-------------------------------------------------------------
print 'Processing non-biographic json'
count = 0
current_folder_path = os.getcwd() #Give the current path
for filename in os.listdir(current_folder_path+'/data/processed_json/'):
    print 'File:'+str(count),filename
    
    with open(current_folder_path+'/data/processed_json/'+filename) as f1:
	    data=f1.read().replace('\n', '')
	
    #data = data.decode('utf8')
    data = data.replace('\'','"')
    data = data.replace('tag','"tag"')
    data = data.replace('content','"content"')
    data = data.replace('s\'','"s\'"')
    data = data.replace('\'s','"\'s"')
    data = data.replace('\'t','"\'t"')
    data = data.replace('""','"')
    path = current_folder_path+'/data/correct_json/non_bio/'
    name = 'non_bio_'+str(count)+'.json'
    total_name = path+name
    with open(total_name,'w') as f2:
        count = count + 1
        try:            
            f2.write(data)               
        except:
            print "Unexpected error: "+name, sys.exc_info()[0]
            continue
    

       
#-------------------------------------------------------------
#     Read the biographic
#-------------------------------------------------------------
print '\nProcessing biographic json'
count_non_bio = 0
current_folder_path = os.getcwd() #Give the current path
folderpath = current_folder_path+'/data/bio_clean/'

#folders = os.listdir(current_folder_path+'/data/bio_clean/')
folders = [d for d in os.listdir(folderpath) if os.path.isdir(os.path.join(folderpath, d))]

#Obtain the filename list
for folder in folders:
    filelist = os.listdir(current_folder_path+'/data/bio_clean/'+folder)
    print '\n'+folder+' with '+str(len(filelist))+' files'
    
    #Obtain the filename list
    for file in filelist:
        print 'File:'+str(count_non_bio),file
        
        with open(current_folder_path+'/data/bio_clean/'+folder+'/'+file) as f1:
	        data=f1.read().replace('\n', '')
		
        #data = data.decode('utf8')
        data = data.replace('\'','"')
        data = data.replace('tag','"tag"')
        data = data.replace('content','"content"')
        data = data.replace('s\'','"s\'"')
        data = data.replace('\'s','"\'s"')
        data = data.replace('\'t','"\'t"')
        data = data.replace('""','"')
        
        path = current_folder_path+'/data/correct_json/bio/'+folder+'/'
        name = 'bio_'+str(count_non_bio)+'.json'
        with open(path+name,'w') as f2:
            count_non_bio = count_non_bio + 1
            try:
                f2.write(data)
            except:
                print "Unexpected error: "+name, sys.exc_info()[0]
                continue
print '-----------------------'
print 'All the correct jsons are in '+current_folder_path+'/data/correct_json'
print '\nWe processing '+str(count_non_bio)+' non-biographic jsons' 
print 'We processing '+str(count)+' biographic jsons'