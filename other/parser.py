import json
from array import *

full_text = ""
json_data=open('test2.json')
data = json.load(json_data)
numSentences = len(data)
word_array = []
tupla = []
for content in data:
    for sentence in content["content"]: 
        #Acceding to a new child of the json
        if len(sentence)==2:
            word = sentence["content"][0][0]
            tupla.append(json.dumps(sentence["content"][0]))
            word_array.append(word)
            full_text = full_text + word + " "
            
        # Case that you access to a simple list
        else:
            word = sentence[0]
            tupla.append(json.dumps(sentence))
            word_array.append(word)
            full_text = full_text + word + " "
#print word_array
#print full_text
#print tupla
with open('array.csv','w') as f:
	for w in word_array:
		try:
			if w!=',' and w!='' and w!='.' and w!=':' and w!='/' and w!='('and w!=')':
				f.write(w.decode('utf8'))
				f.write('\n')
		except:
			continue
		


with open('tuple.csv', 'w') as f:
	for t in tupla:
		#tmp = t.replace('","','')
		tmp = t.replace('","','|')
		tmp = tmp.replace('[','')
		tmp = tmp.replace(']','')
		tmp = tmp.replace('"', '')
		
		if(tmp[1] != ',') and (tmp[1] != '|') and (tmp[1] != ' '):
			f.write(tmp)
			f.write('\n')
			

data = []		
with open('tuple.csv', 'r') as f:
	for line in f.readlines():
		tmp = line.split(',')
		t = []
		for e in tmp:
			t.append(e.replace('\n',''))
		data.append(tuple(t))
		
data = []
with open('array.csv','r') as f:
	for line in f.readlines():
		data.append(line.replace('\n',''))
		
# print to screen 
#from StringIO import StringIO
#io = StringIO(data)
#print json.load(io)
#print '------------\n'
#print json.loads(data)
