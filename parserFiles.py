import json
import os
from array import *

current_folder_path = os.getcwd() #Give the current path
output = ""
#-------------------------------------------------------------
#     Read the biographic json and convert to csv's files
#-------------------------------------------------------------
count = -1
save_array_path = current_folder_path+'/data/csv/bio/array/'
save_tuple_path = current_folder_path+'/data/csv/bio/tuple/'
pathBio = current_folder_path+'/data/INLP_FINAL/Bio_clean/'
#Obtain the folders list in biographical folder
folders = [d for d in os.listdir(pathBio) if os.path.isdir(os.path.join(pathBio, d))]

#For all the folders
for folder in folders:
    filelist = os.listdir(pathBio+folder)
    print '\n\n'+folder+' with '+str(len(filelist))+' files'
    #For all the filename list
    for file in filelist:
        if file.endswith('.json'):    
            full_text = ""
            json_file = pathBio+folder+'/'+file
            count = count + 1
        
            try: 
                #Open and read the json
                json_data=open(json_file)
                data = json.load(json_data)
                
                text = str(count)+" "+file;
                print text
                output = output + text + "\n"
                
                numSentences = len(data)
                word_array = []
                tupla = []
                for content in data:
                    for sentence in content["content"]: 
                        #Acceding to a new child of the json
                        if len(sentence)==2:
                            word = ""
                            for i in range(len(sentence["content"])):
                                word = word+sentence["content"][i][0]+" "
                            #Filter the words with only one character
                            if (len(word)!=1 and word!='``'): 
                                tupla.append(json.dumps(sentence["content"][0]))
                                word_array.append(word)
                                full_text = full_text + word + " "
            
                        # Case that you access to a simple list
                        else:
                            if (len(word)!=1 and word!='``'): 
                                word = sentence[0]
                                tupla.append(json.dumps(sentence))
                                word_array.append(word)
                                full_text = full_text + word + " "
                
                #In this variables you have the content
                #print word_array
                #print full_text
                #print tupla
            
                #Save the array of the words into csv.
                with open(save_array_path+'bio_array_'+str(count)+'.csv','w') as f:
        	        for w in word_array:
        		        try:
        			        if w!=',' and w!='' and w!='.' and w!=':' and w!='/' and w!='('and w!=')':
        				        f.write(w.decode('utf8'))
        				        f.write('\n')
        		        except:
        			        continue
		

                #Save the tuple (word,tag,stem) of each word into csv.
                with open(save_tuple_path+'bio_tuple_'+str(count)+'.csv', 'w') as f:
        	        for t in tupla:
        		        tmp = t.replace('","','|')
        		        tmp = tmp.replace('[','')
        		        tmp = tmp.replace(']','')
        		        tmp = tmp.replace('"', '')
		
        		        if(tmp[1] != ',') and (tmp[1] != '|') and (tmp[1] != ' '):
        			        f.write(tmp.decode('utf8'))
        			        f.write('\n')
            
            except:
                text = 'ERROR: Json is not build correctly '+str(count)+" "+file; 
                print text;
                output = output + text + "\n"    
                continue
        
print "\n\n\n"
#-------------------------------------------------------------
#     Read the non-biographic json and convert to csv's files
#-------------------------------------------------------------
count_non_bio = -1
save_array_path = current_folder_path+'/data/csv/non_bio/array/'
save_tuple_path = current_folder_path+'/data/csv/non_bio/tuple/'
pathNon_Bio = current_folder_path+'/data/INLP_FINAL/NonBio_processed_json/'

#For all the filename list
for filename in os.listdir(pathNon_Bio):
    if filename.endswith('.json'):    
        full_text = ""
        json_file = pathNon_Bio+filename
        count_non_bio = count_non_bio + 1
        
        try: 
            #Open and read the json
            json_data=open(json_file)
            data = json.load(json_data)
            
            text = str(count_non_bio)+" "+filename;
            output = output + text + '\n'
            print text
            numSentences = len(data)
            word_array = []
            tupla = []
            for content in data:
                for sentence in content["content"]: 
                    #Acceding to a new child of the json
                    if len(sentence)==2:
                        word = ""
                        for i in range(len(sentence["content"])):
                            word = word+sentence["content"][i][0]+" "                            
                            #Filter the words with only one character
                        if (len(word)!=1 and word!='``'): 
                            tupla.append(json.dumps(sentence["content"][0]))
                            word_array.append(word)
                            full_text = full_text + word + " "
        
                    # Case that you access to a simple list
                    else:
                        if (len(word)!=1 and word!='``'): 
                            word = sentence[0]
                            tupla.append(json.dumps(sentence))
                            word_array.append(word)
                            full_text = full_text + word + " "
            #In this variables you have the content
            #print word_array
            #print full_text
            #print tupla
            
            #Save the array of the words into csv.
            with open(save_array_path+'non_bio_array_'+str(count_non_bio)+'.csv','w') as f:
                for w in word_array:
                    try:
                        if w!=',' and w!='' and w!='.' and w!=':' and w!='/' and w!='('and w!=')':
                            f.write(w.decode('utf8'))
                            f.write('\n')
                    except:
                        continue
		
            #Save the tuple (word,tag,stem) of each word into csv.
            with open(save_tuple_path+'non_bio_tuple_'+str(count_non_bio)+'.csv', 'w') as f:
        	    for t in tupla:
        		    tmp = t.replace('","','|')
        		    tmp = tmp.replace('[','')
        		    tmp = tmp.replace(']','')
        		    tmp = tmp.replace('"', '')
                    
		
        		    if(tmp[1] != ',') and (tmp[1] != '|') and (tmp[1] != ' '):
        			    f.write(tmp.decode('utf8'))
        			    f.write('\n')
            
        except:
            text = 'ERROR: Json is not build correctly '+str(count_non_bio)+" "+filename;
            print text            
            output = output + text + '\n'
            continue


#-----------------------------------------------------------------------------------------------
#       Results of the execution (the terminal ouput) save into ouput.txt in the current folder
#-----------------------------------------------------------------------------------------------  
text = '\nThe process are finished!'
print text
output = output + text + '\n'
print current_folder_path+'\output.txt'

with open(current_folder_path+'/output.txt', 'w') as f:  
    f.write(output)



