 This is a test module that produces a list of verbs that come up in the corpus of provided biographical documents.
 The input documents are located under inlp/test/input. The output lists are under inlp/test/output. The documents for
 the same person are merged together and the verbs are extracted in a single document named according to the person's
 name

 To execute the module please run the main.py file