#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import json
import logging

input_dir = "/input"
output_dir = "/output"

current_folder = os.getcwd()
biographies_path = os.path.normpath(current_folder+input_dir)

def load_all_content():
    """ returns a dictionary of:
    person -> list of files -> list of words
    """
    folders = [d for d in os.listdir(biographies_path) if os.path.isdir(os.path.join(biographies_path, d))]
    output = {}
    i = 0
    j = 0
    #For all the folders
    for folder in folders:
        # folder is the name
        folder_path = os.path.normpath(biographies_path+"/"+folder)
        filelist = os.listdir(folder_path)
        logging.info('\n\n'+folder+' with '+str(len(filelist))+' files')
        output_list = []
        #For all the files in folder
        for item in filelist:
            #item is a name of the json file

            if not item.endswith('.json'):
                continue
            json_file = os.path.normpath(folder_path+'/'+item)
            #Open and read the json
            try:
                json_data=open(json_file)
                data = json.load(json_data)

                words = []

                for content in data:
                    for sentence in content["content"]:

                        if type(sentence) is dict: # complex element - a name etc
                            for element in sentence["content"]:
                                words.append(element)
                        else:  # simple word
                            words.append(sentence)
#                         print sentence0
            except ValueError as e:
                logging.warning("Error while reading JSON file " + folder + "/" + item + "\n" + e.message)
            output_list.append(words)

        output[folder] = output_list

    return output

def is_verb(entry):
    """
    Receives an entry in a form: ["word", "POStag", "lemma"]
    Return bool - is it a verb?
    """
    return entry[1][0] == "V"

def verbs_for_person(articles):
    """
    Receives a list of lists of words, in a form as in "is_verb".
    Returns one list of verbs
    """
    return [word[0] for list in articles for word in list if (is_verb(word))]
def verbs_for_all(all_people_data):
    result = dict()
    for (person, data) in all_people_data.iteritems():
        result[person] = verbs_for_person(data)
    return result

def write_results(verbs_for_people):
    """ Saves dictionary of people -> verbs to a structure, where
        names of people -> filenames (.csv)
        verbs - one per line
    """
    output_path = os.path.normpath(current_folder + output_dir)
    for (person, data) in verbs_for_people.iteritems():
        filename = os.path.normpath(output_path + '/' + person + '.txt')
        with open(filename, 'w') as f:
            for word in data:
                try:
                    f.write(word.decode('utf8'))
                    f.write('\n')
                except:
                    continue
        f.close()
        logging.info("Written " + person + " with " + str(len(data)) + " verbs")

if __name__ == "__main__":
    logging.basicConfig(level = logging.INFO)
    all_words = load_all_content()
    results = verbs_for_all(all_words)
    write_results(results)
    logging.info("DONE")